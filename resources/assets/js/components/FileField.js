import { Component, h } from 'preact';

export default class FileField extends Component {
    constructor(props) {
        super(props);
        this.uniqueId = Math.random().toString(36).substring(2);
        this.state = {
            filename: null
        }

        this.onChange = this.onChange.bind(this);
        this.onClear = this.onClear.bind(this);
    }

    onClear() {
        this.input.value = null;
        this.setState({
            filename: null
        })
    }

    onChange(ev) {
        this.setState({
            filename: ev.target.value.split( '\\' ).pop()
        })

        if (this.props.onInteract) {
            this.props.onInteract(this.props.name);
        }
    }

    render() {
        return (
            <div className="form__field form__file">

                {
                    this.state.filename ?
                        (
                            <div className="form__file__info">
                                { this.state.filename }
                                <span className="form__file__clear" onClick={this.onClear} title={ this.props.clearTitle || 'Remove file' }>+</span>
                            </div>
                        )
                        : <div className="form__file__info">{ this.props.emptyText || 'No file selected' }</div>
                }

                <div className="form__file__button">
                    <input
                        name={this.props.name}
                        type="file"
                        placeholder={this.props.placeholder}
                        accept={this.props.accept || '*'}
                        id={this.uniqueId}
                        onChange={this.onChange}
                        ref={input => this.input = input}
                    />
                    <label className="button" htmlFor={this.uniqueId}>
                        { this.props.title }
                    </label>
                </div>
            </div>
        )
    }
}
