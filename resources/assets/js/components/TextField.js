import { Component, h } from 'preact';
import classnames from 'classnames';

export default class TextField extends Component {
    constructor(props) {
        super(props);
        this.onKeyPress = this.onKeyPress.bind(this);
    }

    onKeyPress() {
        if (this.props.onInteract) {
            this.props.onInteract(this.props.name);
        }
    }

    render() {
        const className = classnames(
            'form__field',
            {
                'form__field--short': this.props.short,
                'form__field--error': this.props.error
            }
        );

        return (
            <div className={className}>
                {
                    this.props.multiline ?
                        this.renderTextarea() :
                        this.renderTextInput()
                }
                {

                    <span className="form__field__error-text">
                        {
                            this.props.error ?
                                (Array.isArray(this.props.error) ? this.props.error[0] : this.props.error)
                            : null
                        }
                    </span>
                }
            </div>
        )
    }

    renderTextInput() {
        return (
            <input
                name={this.props.name}
                type={this.props.type || 'text'}
                placeholder={this.props.placeholder}
                autoComplete={this.props.name}
                maxLength="255"
                onKeyPress={this.onKeyPress}
            />
        )
    }

    renderTextarea() {
        return (
            <textarea
                name={this.props.name}
                placeholder={this.props.placeholder}
                maxLength="5000"
                onKeyPress={this.onKeyPress}
            />
        )
    }
}
