import { Component, h } from 'preact';
import Form from './Form';
import Post from './Post';

export default class Blog extends Component {
    constructor(props) {
        super(props);

        this.state = {
            quote: props.quote,
            posts: props.posts,
            topWords: props.topWords
        }

        this.storePost = this.storePost.bind(this);
    }

    storePost(post, topWords) {
        const newPosts = this.state.posts.slice();
        newPosts.push(post);
        this.setState({
            posts: newPosts,
            topWords
        });
    }

    render() {
        const sortedPosts = this.state.posts.slice();
        sortedPosts.sort((postA, postB) => {
            return postB.created_at - postA.created_at;
        });

        return (
            <div className="page-index">
                <header className="page-index__header">
                    <h1 className="page-index__header__title">My spectacular blog</h1>
                    <p className="page-index__header__subtitle">{ this.state.quote }</p>
                </header>
                <Form onStorePost={this.storePost} />
                <div className="page-index__content">
                    <div className="page-index__posts">
                        {
                            sortedPosts.map(post => (
                                <Post post={post} key={post.id} />
                            ))
                        }
                    </div>
                    {
                        this.state.topWords.length ? (
                            <div className="page-index__words">
                                <h3 className="page-index__words__title">Most used words here</h3>
                                <ul className="page-index__words__list">
                                    {
                                        this.state.topWords.map(word => (
                                            <li className="page-index__words__list__item" key={word}>
                                                {word}
                                            </li>
                                        ))
                                    }
                                </ul>
                            </div>
                        ) : null
                    }
                </div>
            </div>
        )
    }
}
