import { nl2br } from './helpers';
import { Component, h } from 'preact';

export default class Post extends Component {
    render() {
        const date = new Date(this.props.post.created_at * 1000);
        const dateString = date.toLocaleDateString();

        return (
            <div className="post">
                <header className="post__header">
                    <h2 className="post__header__title">{ this.props.post.title }</h2>
                    <span className="post__header__date">{ dateString }</span>
                </header>
                <div className="post__content">
                    {
                        this.props.post.pictureFilename ?
                            <img
                                className="post__picture"
                                src={`/storage/picture/${this.props.post.pictureFilename}`}
                                alt=""
                            /> : null
                    }
                    {
                        this.props.post.text ?
                            <p className="post__body">
                                { nl2br(this.props.post.text) }
                            </p> : null
                    }
                </div>
            </div>
        )
    }
}
