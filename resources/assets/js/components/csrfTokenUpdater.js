import axios from 'axios';

export default function boot() {
    window.setInterval(() => {
        axios.get('/update_csrf_token');
    }, 60000);
}
