import axios from 'axios';
import classnames from 'classnames';
import FileField from './FileField';
import TextField from './TextField';
import { Component, h } from 'preact';


export default class Blog extends Component {
    constructor(props) {
        super(props);

        this.state = {
            errors: {},
            loading: false
        }

        this.onSubmit = this.onSubmit.bind(this);
        this.onInteract = this.onInteract.bind(this);
    }

    onInteract(name) {
        let resetFieldName = name === 'picture' ? 'text' : name;
        this.setState({
            errors: Object.assign({}, this.state.errors, {[resetFieldName]: null})
        });
    }

    onSubmit(ev) {
        ev.preventDefault();

        if (this.state.loading) {
            return;
        }

        const formData = collectFormData(this.form);
        const errors = validate(formData);
        const hasErrors = Object.keys(errors).length;

        this.setState({ errors, loading: !hasErrors });

        if (!hasErrors) {
            axios({
                method: this.form.method,
                url: this.form.action,
                data: formData
            }).then(response => {
                if (response.data.post) {
                    this.form.reset();
                    this.form.querySelectorAll('input[type=file]').forEach(el => el.dispatchEvent(new Event('change')));

                    if (this.props.onStorePost) {
                        this.props.onStorePost(response.data.post, response.data.topWords);
                    }
                }

                this.setState({ loading: false });
            }).catch(err => {
                if (err.response && (err.response.status === 422)) {
                    this.setState({ errors: err.response.data.errors });
                }

                this.setState({ loading: false });
            });
        }
    }

    render() {
        const className = classnames('form', { 'form--loading': this.state.loading });

        return (
            <form
                className={ className }
                action="/"
                method="post"
                onSubmit={this.onSubmit}
                ref={form => this.form = form}
                noValidate
                encType="multipart/form-data"
            >
                <header className="form__header">
                    New blog post
                </header>
                <div className="form__wrapper">
                    <div className="form__fields">
                        <TextField
                            name="title"
                            placeholder="My post title"
                            error={this.state.errors.title}
                            onInteract={this.onInteract}
                        />
                        <TextField
                            name="text"
                            placeholder="My post text"
                            multiline
                            error={this.state.errors.text}
                            onInteract={this.onInteract}
                        />
                        <TextField
                            name="email"
                            placeholder="E-mail address"
                            type="email"
                            short
                            error={this.state.errors.email}
                            onInteract={this.onInteract}
                        />
                        <FileField
                            name="picture"
                            title="Upload image"
                            accept="image/jpg,image/jpeg,image/png"
                            clearTitle="Remove image"
                            emptyText="No image selected"
                            error={this.state.errors.picture}
                            onInteract={this.onInteract}
                        />
                    </div>

                    <div className="form__actions">
                        <button type="submit" className="button button--primary">
                            Save post
                        </button>
                    </div>
                </div>
                <div className="form__cover">
                    <div className="form__cover__progress"/>
                </div>
            </form>
        )
    }
}



const validate = function(formData) {
    const errors = {};

    if (!formData.get('title')) {
        errors.title = 'Fill the post title';
    }

    if (!formData.get('text') && !formData.get('picture')) {
        errors.text = 'Fill the post text or/and upload an image';
    }

    if (!formData.get('email')) {
        errors.email = 'Fill your e-mail address';
    }

    if (formData.get('email') && !formData.get('email').includes('@')) {
        errors.email = 'Fill the correct e-mail address';
    }

    return errors;
}

const collectFormData = function(form) {
    // There was a bug in Webkit (Safari 11) https://bugs.webkit.org/show_bug.cgi?id=184490
    // Shortly: Sending a form data collected from an empty file input sticks
    // Workaround: disable empty file inputs before collecting the form data

    const fileInputs = form.querySelectorAll('input[type="file"]:not([disabled])');
    fileInputs.forEach(input => {
        if (input.files.length > 0) return;
        input.setAttribute('disabled', '');
    });

    const formData = new FormData(form);

    fileInputs.forEach(input => {
        input.removeAttribute('disabled');
    });

    return formData;
}
