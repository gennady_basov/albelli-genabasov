import axios from 'axios';
import { h, render } from 'preact';
import Blog from './components/Blog';
import csrfTokenUpdater from './components/csrfTokenUpdater';

const blogRoot = document.querySelector('#blog');
if (blogRoot) {
    render(
        <Blog
            quote={window.state.quote}
            posts={window.state.posts}
            topWords={window.state.topWords}
        />,
        blogRoot
    );
}

csrfTokenUpdater();
