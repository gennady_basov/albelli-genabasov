@extends('layout')

@section('content')
    <div id="blog"></div>
@endsection

@push('scripts')
    <script>
        window.state = {
            quote: @json($quote),
            posts: @json($posts),
            topWords: @json($topWords)
        }
    </script>
@endpush
