<?php

namespace App\Services;

use Carbon\Carbon;
use Illuminate\Http\UploadedFile;

/**
 * Blog post list manipulation
 *
 * Class PostRepository
 * @package App\Services
 */
class PostRepository
{
    const MINIMAL_WORD_LENGTH = 4;

    protected $path;
    protected $fileStore;
    protected $imageProcess;

    public function __construct($path, ImageProcess $imageProcess)
    {
        $this->path = $path;
        $this->fileStore = new FileStore($path);
        $this->imageProcess = $imageProcess;
    }

    /**
     * Returns all saved posts
     *
     * @return array mixed
     */
    public function all()
    {
        return $this->fileStore->read('posts', []);
    }

    /**
     * Store new post
     *
     * @param array $data Consists of: title(string), text(string), picture(UploadedFile)
     * @return array
     */
    public function store($data)
    {
        if (@$data['picture'] && ($data['picture'] instanceof UploadedFile))
        {
            $pictureFilename = $this->imageProcess->store($data['picture'], 300, 300);
        }

        $post = [
            'id' => uniqid(),
            'title' => @$data['title'],
            'text' => @$data['text'],
            'pictureFilename' => $pictureFilename ?? null,
            'created_at' => Carbon::now()->getTimestamp()
        ];

        $posts = $this->fileStore->read('posts', []);
        $posts[] = $post;
        $this->fileStore->put('posts', $posts);
        $this->updateWordIndex($post);

        return $post;
    }

    /**
     * Most used words across all the posts (in titles and texts)
     *
     * @param int $count How many words to return
     * @return array [wordA, wordB, ...]
     */
    public function topWords($count = 5)
    {
        $wordIndex = $this->fileStore->read('words', []);
        arsort($wordIndex);
        return array_keys(
            array_slice(
                $wordIndex, 0, $count, true
            )
        );
    }

    /**
     * Update existing popular words index with words from a new post
     *
     * @param array $post
     */
    protected function updateWordIndex($post)
    {
        $update = $this->countWords($post);
        $index = $this->fileStore->read('words', []);

        foreach ($update as $word => $count)
        {
            $index[$word] = ($index[$word] ?? 0) + $count;
        }

        $this->fileStore->put('words', $index);
    }

    /**
     * Count popular words in the post
     *
     * @param array $post
     * @return array [wordA => countA, wordB => countB, ...]
     */
    protected function countWords($post)
    {
        $index = [];

        foreach (['title', 'text'] as $property)
        {
            $string = $post[$property] ?? '';

            preg_match_all('/\w+/', $string,$words);

            foreach ($words[0] as $word)
            {
                $lowercaseWord = mb_strtolower($word);

                if (mb_strlen($lowercaseWord) >= static::MINIMAL_WORD_LENGTH)
                {
                    $index[$lowercaseWord] = ($index[$lowercaseWord] ?? 0) + 1;
                }
            }
        }

        return $index;
    }
}
