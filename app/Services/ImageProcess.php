<?php

namespace App\Services;

use Storage;
use Illuminate\Http\UploadedFile;

/**
 * Image resizing and storing in a filesystem
 *
 * Class ImageProcess
 * @package App\Services
 */
class ImageProcess
{
    const FILENAME_LENGTH = 16;

    protected $storagePath;

    /**
     * @param string $path Storage directory
     */
    public function __construct($path)
    {
        $this->storagePath = $path;
    }

    /**
     * Resize uploaded image to the specified size and store it in a filesystem
     *
     * @param UploadedFile $file
     * @param int $targetWidth
     * @param int $targetHeight
     * @return null|string
     */
    public function store(UploadedFile $file, $targetWidth = 300, $targetHeight = 300)
    {
        switch ($file->getMimeType())
        {
            case 'image/jpeg':
                $image = imagecreatefromjpeg($file->getPathname());
                break;
            case 'image/png':
                $image = imagecreatefrompng($file->getPathname());
                break;
            default:
                $image = null;
        }

        if (!$image)
        {
            return null;
        }

        $sourceWidth = imagesx($image);
        $sourceHeight = imagesy($image);

        $sourceRatio = $sourceWidth / $sourceHeight;
        $targetRatio = $targetWidth / $targetHeight;

        if ($sourceRatio > $targetRatio)
        {
            $scale = $targetHeight / $sourceHeight;
            $cropX = ($sourceWidth * $scale - $targetWidth) / 2;
            $cropY = 0;
        }
        else
        {
            $scale = $targetWidth / $sourceWidth;
            $cropX = 0;
            $cropY = ($sourceHeight * $scale - $targetHeight) / 2;
        }

        $image = imagescale($image, $sourceWidth * $scale, $sourceHeight * $scale);
        $image = imagecrop($image, [
            'x' => $cropX,
            'y' => $cropY,
            'width' => $targetWidth,
            'height' => $targetHeight
        ]);

        ob_start();
        imagejpeg($image, null, 80);
        $image = ob_get_contents();
        ob_end_clean();

        if ($image)
        {
            $filename = str_random(static::FILENAME_LENGTH) . '.jpg';
            Storage::put($this->storagePath .'/'. $filename, $image);
            return $filename;
        }
        else
        {
            return null;
        }
    }
}
