<?php

namespace  App\Services;

use Storage;
use Illuminate\Contracts\Filesystem\FileNotFoundException;

/**
 * Provides a simple interface for storing key-value data in a single JSON encoded file
 *
 * Class FileStore
 * @package App\Services
 */
class FileStore
{
    protected $path;

    /**
     * @param string $path Path where to store the file
     */
    public function __construct($path)
    {
        $this->path = $path;
    }

    /**
     * Read specific value from the file
     *
     * @param string $key
     * @param mixed $default
     * @return mixed
     */
    public function read($key, $default)
    {
        $data = $this->getFileContent();
        return $data[$key] ?? $default;
    }

    /**
     * Save a value to the file
     *
     * @param string $key
     * @param mixed $value
     */
    public function put($key, $value)
    {
        $data = $this->getFileContent();
        $data[$key] = $value;
        $this->saveFileContent($data);
    }

    /**
     * Returns all the data from the file, decoded from JSON
     *
     * @return array
     */
    protected function getFileContent()
    {
        try
        {
            $fileContent = Storage::get($this->path);
        }
        catch(FileNotFoundException $exception)
        {
            $fileContent = '[]';
        }

        try
        {
            return json_decode($fileContent, true) ?? [];
        }
        catch (\Exception $e)
        {
            return [];
        }
    }

    /**
     * Update the file with new data
     *
     * @param array $data
     */
    protected function saveFileContent($data)
    {
        $fileContent = json_encode($data, config('app.debug') ? JSON_PRETTY_PRINT : 0);
        Storage::put($this->path, $fileContent);
    }
}
