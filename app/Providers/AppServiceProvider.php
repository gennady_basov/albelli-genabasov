<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('App\Services\PostRepository', function($app) {
            return new \App\Services\PostRepository(
                config('blog.post_repository_path'),
                $app->make('App\Services\ImageProcess')
            );
        });

        $this->app->singleton('App\Services\ImageProcess', function() {
            return new \App\Services\ImageProcess(config('blog.pictures_path'));
        });
    }
}
