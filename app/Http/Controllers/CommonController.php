<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CommonController extends Controller
{
    public function update_csrf_token()
    {
        return 'ok';
    }
}
