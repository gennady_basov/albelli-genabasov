<?php

namespace App\Http\Controllers;

use App\Services\ImageProcess;
use App\Services\PostRepository;
use Illuminate\Foundation\Inspiring;
use App\Http\Requests\Blog\StoreRequest;

class BlogController extends Controller
{
    const TOP_WORDS_COUNT = 5;

    protected $imageProcess;
    protected $postRepository;

    public function __construct(PostRepository $postRepository, ImageProcess $imageProcess)
    {
        $this->postRepository = $postRepository;
        $this->imageProcess = $imageProcess;
    }

    /**
     * Index page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $posts = $this->postRepository->all();
        $topWords = $this->postRepository->topWords(static::TOP_WORDS_COUNT);
        $quote = explode('.', Inspiring::quote())[0];

        return view('home', compact('posts', 'topWords', 'quote'));
    }

    /**
     * Blog post store endpoint
     * @param StoreRequest $request
     * @return array
     */
    public function store(StoreRequest $request)
    {
        $post = $this->postRepository->store(
            $request->only('title', 'text', 'picture')
        );
        $topWords = $this->postRepository->topWords(static::TOP_WORDS_COUNT);

        return compact('post', 'topWords');
    }
}
