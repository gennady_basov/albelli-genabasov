<?php

namespace App\Http\Requests\Blog;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'title' => 'required',
            'text' => 'required_without:picture',
            'email' => [
                'required',
                'email',
                Rule::in([config('blog.owner_email')])
            ],
            'picture' => 'nullable|image'
        ];
    }
}
