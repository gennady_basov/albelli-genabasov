<?php

Route::get('/', 'BlogController@index');
Route::post('/', 'BlogController@store');

Route::get('update_csrf_token','CommonController@update_csrf_token');
