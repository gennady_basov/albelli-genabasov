# albelli tech assignment #

Dear reviewer,

Thank you for this opportunity. Implementing the task was quite an interesting challenge.

## Framework choice ##

- PHP framework here is Laravel 5.5. I use Laravel for most of my PHP projects;
- JS framework is Preact. I think it's a great choice for tiny projects;
- No CSS framework (excepting normalize.css).

## Requirements ##

- PHP 7.* (mbstring, gd)
- Composer
- Or just Docker 18.0.2+ with Docker Compose

## How to run ##

- `git clone git@bitbucket.org:gennady_basov/albelli-genabasov.git`
- `cd albelli-genabasov`
- Run it on your local machine directly:
  - `composer install`
  - `php artisan serve --host=127.0.0.1 --port=8000`
- Or use Docker:
  - `docker-compose up`
- Go to `http://127.0.0.1:8000`
- Use `admin@example.com` as the admin e-mail address. You can change it inside `.env` file (server restart is needed).


## Where the actual code is

- Controller: `app/Http/Controller/BlogController.php`
- Other PHP logic: `app/Services`
- Styles and JS: `resources/assets`

## Notes ##

- You may find CSS and JS builds are saved in the repository. I did it deliberately to eliminate the building stage (and hopefully save your time);
- In order to prevent using a database I store the content inside a JSON-encoded file which is located in `storage/app/posts.js`.
