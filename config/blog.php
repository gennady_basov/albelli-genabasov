<?php

return [
    'owner_email' => env('OWNER_EMAIL', 'admin@example.com'),
    'post_repository_path' => 'posts.json',
    'pictures_path' => 'public/picture'
];
