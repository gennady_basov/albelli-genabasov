FROM nginx:1.13

COPY docker/vhost.conf /etc/nginx/conf.d/default.conf

WORKDIR /var/www

COPY public public
