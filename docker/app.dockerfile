FROM php:7.1-fpm

RUN export CFLAGS="$PHP_CFLAGS" CPPFLAGS="$PHP_CPPFLAGS" LDFLAGS="$PHP_LDFLAGS" \
    && apt-get update \
    && apt-get install -y --no-install-recommends \
        git \
        zip unzip \
        libfreetype6-dev \
        libjpeg-dev \
        libpng-dev \
    && rm -rf /var/lib/apt/lists/* \
    && docker-php-ext-install mbstring \
    && docker-php-ext-configure gd \
            --with-freetype-dir=/usr/include/ \
            --with-jpeg-dir=/usr/include/ \
    && docker-php-ext-install -j$(nproc) gd

WORKDIR /var/www

RUN curl -s https://getcomposer.org/composer.phar > /usr/local/bin/composer \
    && chmod a+x /usr/local/bin/composer
ENV COMPOSER_ALLOW_SUPERUSER 1

COPY composer.json ./

RUN composer install --no-scripts --no-autoloader

COPY docker/php.ini /usr/local/etc/php/conf.d/custom.ini

COPY . ./

RUN chown -R www-data:www-data /var/www/storage

RUN composer dump-autoload --optimize

RUN php -r "file_exists('.env') || (copy('.env.example', '.env') && exec('php artisan key:generate'));"

RUN php artisan storage:link
